@extends('layouts.master')

@section('pagecontent')
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open()  !!}
    <div class="form-group">
        {!! Form::label('title','Cikk címe*') !!}
        {!! Form::text('title',null,['class="form-control"']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('lead','Cikk bevezető') !!}
        {!! Form::textarea('lead',null,['class="form-control" rows="5"']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('body','Cikk tartalom*') !!}
        {!! Form::textarea('body',null,['class="form-control"']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('author','Szerző') !!}
        {!! Form::text('author',null,['class="form-control"']) !!}
    </div>
    {!! Form::submit('submit',['class="btn btn-primary"']) !!}
    {!! Form::close() !!}
@stop