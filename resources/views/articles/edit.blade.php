@extends('layouts.master')

@section('pagecontent')
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($article)  !!}
    <div class="form-group">
        {!! Form::label('title','Cikk címe*') !!}
        {!! Form::text('title',old('title',$article->title),['class="form-control"']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('lead','Cikk bevezető') !!}
        {!! Form::textarea('lead',old('lead',$article->lead),['class="form-control" rows="5"']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('body','Cikk tartalom*') !!}
        {!! Form::textarea('body',old('body',$article->body),['class="form-control"']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('author','Szerző') !!}
        {!! Form::text('author',old('title',$article->author),['class="form-control"']) !!}
    </div>
    {!! Form::submit('submit',['class="btn btn-primary"']) !!}
    {!! Form::close() !!}
@stop