@extends('layouts.master')

@section('pagecontent')
    <a href="{{route('article-create')}}" class="btn btn-primary">új cikk</a>
    <table class="table table-responsive table-striped">
        <tr>
            <th>id</th>
            <th>cím</th>
            <th>szerző</th>
            <th>megjelenés</th>
            <th>művelet</th>
        </tr>
        @foreach($articles as $article)
            <tr>
                <td>{{$article->id}}</td>
                <td>{{$article->title}}</td>
                <td>{{$article->author}}</td>
                <td>{{$article->publish_on}}</td>
                <td><a class="btn btn-warning" href="{{route('article-edit',$article->id)}}">szerkeszt</a> <a href="{{route('article-delete',$article->id)}}" class="btn btn-danger">töröl</a></td>
            </tr>
        @endforeach
    </table>
@stop