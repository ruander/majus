<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!--saját stílusok-->
    <link rel="stylesheet" href="css/custom.css">
    <title>Első laravel projektem @yield('pagetitle')</title>
</head>
<body>
<nav class="navbar navbar-inverse bg-inverse navbar-toggleable-md">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('about') }}">Rólam{{--<span class="sr-only">(current)</span>--}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('contact-us') }}">Kapcsolat</a>
        </li>

    </ul>
</nav>
<div class="container">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
            @endauth
        </div>
    @endif

    @yield('pagecontent')

</div>


</body>
</html>
