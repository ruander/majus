@extends('layouts.master')

@section('pagetitle','| Contact Us')

@section('pagecontent')
    Kapcsolat űrlap

    {{--{!! dump( $errors )!!}--}}
    {{--hibák ha vannak kijelzése--}}
    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif
    {!! Form::open() !!}

    {!! Form::label('name', 'Név'); !!}
    {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Gipsz Jakab']) !!}

    {!! Form::label('email', 'Email'); !!}
    {!! Form::text('email',null,['class'=>'form-control', 'placeholder'=>'test@email.cim']) !!}

    {!! Form::label('msg', 'Üzenet'); !!}
    {!! Form::textarea('msg',null,['class'=>'form-control', 'placeholder'=>'üzenet min 50 karakter']) !!}

    {!! Form::submit('Küldés',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}




@endsection
