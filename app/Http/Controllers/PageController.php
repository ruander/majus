<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database;
class PageController extends Controller
{
    //about
    function about(){//adat átadása viewnak
        $firstName = 'Gyuri';
        $lastName = 'Horváth';
        return view('about',compact('firstName','lastName'));
    }
    //contact
    public function contactUs()
    {
        return view('contact-us');
    }

    public function send(Request $request){
        //validálási szabályok
        $rules = [
            'name' => 'required|min:3',
            'email'=>'required|email',
            'msg'=>'required|min:50|max:2000'
        ];
        //custom üzenetek pl:
        $messages = [

                'required' => 'Tőcsed ki'
        ];
        $validatedData = $request->validate($rules,$messages);
        //ide csak valid adatok esetén jutunk
        dd($request->all());
    }
}
