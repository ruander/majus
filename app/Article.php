<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //'űrlapból írható' mezők
   protected $fillable = [
     'title','lead','body','author','publish_on'
   ];
}
