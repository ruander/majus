<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//egyszerű route feldolgozás, callback visszatéréssel
Route::get('valami',function (){
   return 'Hello World - '.date('Y-m-d H:i:s');
});
Route::get('about','PageController@about')->name('about');
Route::get('contact-us','PageController@contactUs')->name('contact-us');
//kapcsolat űrlap feldolgozása
Route::post('contact-us','PageController@send')->name('contact-send');
//cikkek
Route::get('articles','ArticlesController@index')->name('articles-list');
Route::get('articles/new','ArticlesController@create')->name('article-create');
Route::post('articles/new','ArticlesController@store');
Route::get('articles/edit/{id}','ArticlesController@edit')->name('article-edit');
Route::post('articles/edit/{id}','ArticlesController@update')->name('article-update');
Route::get('articles/delete/{id}','ArticlesController@destroy')->name('article-delete');